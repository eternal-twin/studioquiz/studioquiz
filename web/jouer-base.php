<?php 
//session cross to sub domain
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 
if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}
else
{
  //GO LOGIN
  header('Location: login.php');
  exit();
}	

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}

$avatar_credential = rand(100,300);

// Enregistrement de l'utilisateur à jour
include('config.php');

try
{

	$uuid = $_SESSION['userid_dq'];
	$pseudo = $_SESSION['pseudo_dq'];

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

	if (!$mysqli->query("Call DQ_InsertStatsUsers('".$uuid."','".$pseudo."');")) {
		echo "Echec de la requête : " . $mysqli->error;
	}
	else
	{
		$file = 'images/avatar/0.jpg';
		$newfile = 'images/avatar/'.$uuid.'.jpg';

		if (!file_exists($newfile)) copy($file, $newfile);		
	}
	
	$mysqli = null;

}
catch (Exception $e)
{
        die('Erreur : ' . $e->getMessage());
}

function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta https-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz - Jouer une partie</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- DirectQuiz Choix Salon -->
    <link rel="stylesheet" href="css/style-sq-home.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	
	<link rel="stylesheet" href="css/shaker.css">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
	
		
		#choixPrincipal
		{
			cursor: pointer;
			top: 50%;
			left: 50%; 
			transform: translate(-50%, -50%); 
		}
		
		.vignette-jouer
		{
			border-radius : 10px 10px 10px 10px;
			box-shadow: 0px 0px 10px #000;
			border: 5px solid white;
			margin-right: 10px;
			margin-top: 20px;
			width: 210px;
			height: 215px;
			cursor: pointer;
		}
		
		.vignette-jouer:hover
		{
			box-shadow: 0px 0px 10px #000;
			border: 5px solid #FE9;
		}
		
		#tooltip
		{
			background-color: #FBF;
			color: #007;
			padding: 5px;
			font-weight: bold;
			border-radius: 4px 4px 4px 4px;
			position: absolute;
			z-index: 900;
			left: -100px;
			top: -100px;
		}
		
		.marquee {
		  width: 100%; /* the plugin works for responsive layouts so width is not necessary */
		  overflow: hidden;
		}

		#grid
		{
			padding-left: 30px;
		}

	</style>


	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

    <!-- ALL JS FILES -->
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>	
    <script src="https://directquiz.niko.ovh/dev/js/jquery.tightgrid.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.Marquee/1.6.0/jquery.marquee.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js" integrity="sha512-0QbL0ph8Tc8g5bLhfVzSqxe9GERORsKhIn1IrpxDAgUsbBGz/V7iSav2zzW325XGd1OMLdL4UiqRJj702IeqnQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdn.jsdelivr.net/npm/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>

	<script>

function onlineEntry(theme, online)
{
	this.theme = theme;
	this.online = online;
}

var audioFlap  = new Audio('https://directquiz.niko.ovh/dev/flap.mp3');

var joueursOnline = new Array(0);

<?php include('niko.ovh/directquiz89/getAllJoueursOnlineByTheme.php') ?>

		function recupererLesThemes()
		{

			$.get("niko.ovh/directquiz89/getAllThemes.php").done(function(data){

				// let themeDesordre = data.sort(() => Math.random() - 0.5);

				data.forEach(elem => {
					
					$("#grid").append(`<img class="vignette-jouer" id="jouer-${elem.Mnemonic}" online="" theme-id="${elem.IDTheme}" title-data="${elem.LibelleTheme}" alt="${elem.LibelleTheme}" src="images/${elem.Mnemonic}.png" />`);

					$(`#jouer-${elem.Mnemonic}`).click(function(){

									audioFlap.play();

									var elementPrincipal = $(this).clone();

									$(elementPrincipal).attr("id","choixPrincipal");

									$(elementPrincipal).appendTo("#plateau");

									$(elementPrincipal).css("position","absolute");
									$(elementPrincipal).css("width","100px");
									$(elementPrincipal).css("height","102px");
									$(elementPrincipal).css("left","47vw");
									$(elementPrincipal).css("top","47vh");

									$(elementPrincipal).animate({	
										width: "600px",
										height: "614px",
										left: "47vw",
										top: "47vh"
									}, 850, function() {								
											
											setTimeout(openPartie(elem.Mnemonic), 1900);
											
									});

					});

				});

						$(".vignette-jouer").each(function(){
						
								var indexOfOnlineEntry = joueursOnline.map(function(e) { return e.theme; }).indexOf(parseInt($(this).attr("theme-id"),10));
								
								if ($(this).attr("online") != "off")
								$(this).attr("online"," ("+joueursOnline[indexOfOnlineEntry].online + " / " + "8" + " joueur(s) en ligne)");
								
								$(this).mousemove(function(event){

									$("#tooltip").text($(this).attr("title-data") + (($(this).attr("online") != "off")?$(this).attr("online"):"") );
									
									$("#tooltip").offset({ top: event.pageY+15, left: event.pageX+15 });
								
								});
								
								$(this).mouseleave(function(event){
									
									$("#tooltip").hide();
									
								});
								
								$(this).mouseenter(function(event){
									
									$("#tooltip").show();
									
								});
			
						});  

						var grid = document.querySelector('#grid');
					
						var msnry = new Masonry(grid, {
							itemSelector: '.vignette-jouer',
							columnWidth: '.vignette-jouer', // Optionnel : largeur de référence
							percentPosition: true, // Pour une mise en page fluide
						});

			});

		}

		$(function(){
			
			$('.marquee').marquee('destroy');
			$('.marquee').marquee({
					//duration in milliseconds of the marquee
					duration: 8200,
					//gap in pixels between the tickers
					gap: 50,
					//time in milliseconds before the marquee will start animating
					delayBeforeStart: 0,
					//'left' or 'right'
					direction: 'left',
					//true or false - should the marquee be duplicated to show an effect of continues flow
					duplicated: true
				});	
				

			recupererLesThemes();

					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
			
		});
		
		function openPartie(mode)
		{
			document.location = mode+'.php?v=<?= guidv4(); ?>';
		}
		
	</script>
	
</head>

<body id="about_us" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

<div id="tooltip"></div>

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php?v=5">Accueil</a></li>
                      
						<li><a class="nav-link active" href="jouer.php?v=5">Rejoindre une partie</a></li>
						
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
												<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
	
					 <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					   <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
					
                    </ul>
                </div>
                <div class="search-box">

                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->
	

    <!-- section -->
    <div id="fond-public" class="section layout_padding theme_bg">
        <div class="container">
        
<div id="my-toast-location" style="position: absolute; top; 10px; left: 10px;"></div>
<div id="sender">


<div id="plateau">
<center><h1>Choix du mode de jeu</h1></center>
<div id="grid">
	
</div>

	<div id="cadreDefilement"><div class="marquee"><p id="defilement">Directquiz : Le quiz en DIRECT ! Venez participer dans la catégorie de votre choix ! Testez votre culture générale sur 10 questions... <strong>🔆 NOUVEAU 🔆</strong> : Salon sur le thème des <strong>sciences</strong> ! Venez tester vos connaissances dans ce domaine !</p></div></div>

</div> <!-- plateau -->
</div> <!-- sender -->

<br/><br/>


               
      
        </div>
    </div>
    <!-- end section -->

	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

</body>

</html>