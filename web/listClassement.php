 <?php

 try
 {
 
 header('Content-Type: application/json');

 include 'config.php';

$order = $_POST['order'][0]; // Première colonne de tri

$columnIndex = $order['column']; // Indice de la colonne à trier
$columnDirection = $order['dir']; // Direction du tri (asc ou desc)

// Récupérer les paramètres de pagination
$start = $_POST['start']; // Indice de départ pour la pagination
$length = $_POST['length']; // Nombre de lignes par page

 file_put_contents("error.txt", "START=".$start." LENGTH=".$length);

$draw = isset($_POST['draw']) ? intval($_POST['draw']) : 0;
	
$searchValue = $_POST['search']['value'];	
	

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	
	$query = "CALL `DQ_GetCustomStatsUsersServerSide`($columnIndex, '$columnDirection', $start, $length, '$searchValue');";
	$reponse = $mysqli->query($query);
	
	$tabData = array();

	$total_count=1;
	$total_count_vierge=1;
	
	$cpt = 0;
	
    while ($row = $reponse->fetch_array(MYSQLI_ASSOC)) {
		
		$cpt++;
		
		$total_count = $row['total_count'];
		$total_count_vierge = $row['total_count_vierge'];
		
			if ($row['GameDone'] < 5)
			{
				$currentLevel = 0;
			}
			else if ($row['GameDone'] < 50)
			{
				$currentLevel = 1;
			}
			else if ($row['GameDone'] < 150)
			{
				$currentLevel = 2;
			}
			else if ($row['GameDone'] < 300)
			{
				$currentLevel = 3;
			}
			else if ($row['GameDone'] < 600)
			{
				$currentLevel = 4;
			}
			else if ($row['GameDone'] < 900)
			{
				$currentLevel = 5;
			}
			else if ($row['GameDone'] < 1200)
			{
				$currentLevel = 6;
			}
			else if ($row['GameDone'] < 2000)
			{
				$currentLevel = 7;
			}
			else if ($row['GameDone'] < 4000)
			{
				$currentLevel = 8;
			}
			else
			{
				$currentLevel = 9;
			}	

			$moyenne = round($row['Moyenne'], 0, PHP_ROUND_HALF_UP); 

			$lastGame = 'Date inconnue';
			if (!is_null($row['LastGame']))
			{
				$lastGame = strtotime($row['LastGame']);
				$lastGame = date('d/m/Y H:i:s', $lastGame);
			}									
		
			array_push($tabData, array( 
			  "position" => ($row['position']==1?'🏆 '.$row['position']:($row['position']==2?'🥈 '.$row['position']:($row['position']==3?'🥉 '.$row['position']:'🍫 '.$row['position']))),
			  "level" => $currentLevel,
			  "avatar" => $row['UUID'].'.jpg?'.time(),
			  "pseudo" => $row['Pseudo'],
			  "points_totaux" => $row['PointsTotaux'],
			  "best_score" => $row['BestScore'],
			  "moyenne" => $moyenne,
			  "game_done" => $row['GameDone'],
			  "last_game" => $lastGame  
			));

    }


	if (sizeof($tabData) == 0)
	{
			array_push($tabData, array( 
			  "position" => "",
			  "level" => "",
			  "avatar" => "",
			  "pseudo" => "Aucun joueur trouvé !",
			  "points_totaux" => "",
			  "best_score" => "",
			  "moyenne" => "",
			  "game_done" => "",
			  "last_game" => ""  
			));
	}


	$tabJson = array("draw"=>$draw, "recordsTotal"=> $total_count_vierge , "recordsFiltered"=> $total_count, "data"=> $tabData);

	file_put_contents("debug0.txt", json_encode($tabJson));

	echo json_encode($tabJson);

        $reponse->free();
        /* Fermeture de la connexion */
 }
 catch (Exception $ex) 
 {
	 file_put_contents("error.txt", $ex->getMessage());
 }
 finally
 {
	    $mysqli->close();
        $mysqli = null;
 }
	
?>