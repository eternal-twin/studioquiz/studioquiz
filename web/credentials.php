<?php

require 'vendor/autoload.php';
use Etwin\OauthClient\RfcOauthClient;

 $authorizationEndpoint = "http://nodejs.niko.ovh:50320/oauth/authorize";
 $tokenEndpoint = "http://nodejs.niko.ovh:50320/oauth/token";
 $callbackEndpoint = "http://directquiz-test.niko.ovh/callback.php";
 $clientId = "directquiz@clients";
 $clientSecret = "dev_secret";

$oauthClient = new RfcOauthClient(
$authorizationEndpoint,
$tokenEndpoint,
$callbackEndpoint,
$clientId,
$clientSecret
);

?>