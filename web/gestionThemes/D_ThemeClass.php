<?php
// Inclure le fichier de configuration
include 'config.php';

// Configurer les en-têtes pour la réponse JSON
header('Content-Type: application/json');

try {
    // Vérifier si les paramètres 'idMapping' et 'idTheme' sont fournis et valides
    if (!isset($_GET['idMapping']) || !is_numeric($_GET['idMapping'])) {
        throw new Exception("Le paramètre 'idMapping' est manquant ou invalide.");
    }
    if (!isset($_GET['idTheme']) || !is_numeric($_GET['idTheme'])) {
        throw new Exception("Le paramètre 'idTheme' est manquant ou invalide.");
    }

    // Récupérer les paramètres
    $p_id_mapping = (int)$_GET['idMapping'];
    $p_theme = (int)$_GET['idTheme'];

    // Connexion à la base de données
    $dsn = "mysql:host=$ADRES;dbname=$BASE;charset=utf8mb4";
    $pdo = new PDO($dsn, $USER, $MDP);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Appeler la procédure stockée avec les paramètres
    $stmt = $pdo->prepare("CALL DQ_DeleteThemeClassification(:p_id_mapping, :p_theme)");
    $stmt->bindParam(':p_id_mapping', $p_id_mapping, PDO::PARAM_INT);
    $stmt->bindParam(':p_theme', $p_theme, PDO::PARAM_INT);
    $stmt->execute();

    // Vérifier le succès de l'opération
    $stmt->closeCursor();

    // Retourner une réponse JSON indiquant le succès
    echo json_encode(['success' => true, 'message' => "Classification supprimée avec succès."]);
} catch (PDOException $e) {
    // Gestion des erreurs de base de données
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
} catch (Exception $e) {
    // Gestion des erreurs générales
    echo json_encode(['success' => false, 'error' => $e->getMessage()]);
}
?>
