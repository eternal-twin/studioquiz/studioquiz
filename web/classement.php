<?php 
//session cross to sub domain
//ini_set('session.cookie_domain', substr($_SERVER['SERVER_NAME'],strpos($_SERVER['SERVER_NAME'],"."),100));
session_start(); 

if (!empty($_COOKIE["userid_dq"]))
{
	$_SESSION["userid_dq"] = $_COOKIE["userid_dq"];
}

if (!empty($_COOKIE["pseudo_dq"]))
{
	$_SESSION["pseudo_dq"] = $_COOKIE["pseudo_dq"];
}
?>
<!DOCTYPE html>
<html lang="fr">
<!-- Basic -->

<head>
	<base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>Directquiz</title>
	<link rel="manifest" href="/manifest.json">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="#" type="image/x-icon" />
    <link rel="apple-touch-icon" href="#" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Pogo Slider CSS -->
    <link rel="stylesheet" href="css/pogo-slider.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
	<!-- Datatable CSS -->
    <link rel="stylesheet" href="css/data-table.css">

	<style>
		th
		{
			
			font-size: 0.8em!important;
			
		}
		
	</style>
		
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>	
<script src="https://cdn.datatables.net/1.13.11/js/jquery.dataTables.min.js"></script>

	<script>
	
	$(function(){
				
				
					$.post("php/getLevelByUUID.php",{uuid:'<?= $_SESSION['userid_dq'] ?>'}).done(function(data){

						var result = data.split("#");
						//Level # UUID
	
						if (result[1]!="")
						{
							$(".ceinture2").html("<img class='ceinture-profil-menu' src='images/ceinture_"+result[0].trim()+".png' />");
						}
						
						if (result[3]!="")
						{
							$("#directdollar-menu").text(result[3].trim());
						}
					}).
					fail(function(){
						
						
						
					});
				
		
		setJqueryDataTable();
		
	});
	
	        function setJqueryDataTable() {

            $("#classement").DataTable({
				order: [2, "desc"],
				pageLength: 35,
                serverSide: true,
                processing: true,
				paging  : true,
				dom: 'Brftip',
				language: {
					url: "js/DT_French.json"
				},
                ajax: {
                    url: 'listClassement.php', // Lien vers le script PHP qui renvoie les données
                    type: 'POST',
					datatype: 'json',					
					dataSrc: function(response) {
						return response.data;
					},
					error: function(xhr, errorType, exception) {
						console.error("Erreur AJAX : " + xhr.status + " - " + exception);
					}
                },
				columnDefs: [
				{
					"data": 'position', 
					"title":'#',
                    "targets": [0],
                    "orderable": false,
                    "searchable": false,
					"width" : 60
                },
                {
					"data": 'pseudo', 
					"title":'Joueur',
                    "targets": [1],
                    "orderable": true,
                    "searchable": true,
					"width" : 235,
					"render": function (data, type, row) {

						return '<img alt="avatar" class="avatar-classement" src="images/avatar/'+row.avatar+'" /><img alt="grade" class="avatar-classement-level" src="images/avatar/level_' + row.level + '.png" /> <span style="position: relative; left:-35px;">' + row.pseudo + '</span>';
	
					}
                },
			    {
					"data": 'points_totaux', 
					"title":'Points totaux',
                    "targets": [2],
                    "orderable": true,
                    "searchable": false,
                    "width" : 80
                },
                {
					"data": 'best_score', 
					"title":'Meilleur score',
                    "targets": [3],
                    "orderable": true,
                    "searchable": false,
                    "width" : 80
                },
				{
					"data": 'moyenne', 
					"title":'Score moyen',
                    "targets": [4],
                    "orderable": true,
                    "searchable": false,
                    "width" : 80
                },
                {
					"data": 'game_done', 
					"title":'Parties jouées',
                    "targets": [5],
                    "orderable": true,
                    "searchable": false,
                    "width" : 80
                },
				{
					"data": 'last_game', 
					"title":'Dernière partie',
                    "targets": [6],
                    "orderable": false,
                    "searchable": false,
                    "width" : 150
                }
                ],
				    lengthMenu: [10, 15] // Options de sélection du nombre de lignes par page				
            }).on('draw',function(){
			
				// Do nothing
			
			});
			
        }
		
		</script>
	
</head>

<body id="home" data-spy="scroll" data-target="#navbar-wd" data-offset="98">

    <!-- LOADER -->
    <div id="preloader">
        <div class="loader">
            <img src="images/loader.gif" alt="#" />
        </div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    <!-- Start header -->
    <header class="top-header">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="image"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-wd" aria-controls="navbar-wd" aria-expanded="false" aria-label="Toggle navigation">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbar-wd">
                    <ul class="navbar-nav">
                        <li><a class="nav-link" href="index.php">Accueil</a></li>
              
						<li><a class="nav-link" href="jouer.php">Rejoindre une partie</a></li>
						<?php if (!isset($_SESSION['userid_dq'])) { ?>
                        <li><a class="nav-link" href="login.php">Inscription / Connexion</a></li> <!-- Script qui reviendra à jouer -->
						<?php } ?>
						<li><a class="nav-link active" href="classement.php">Classement</a></li>
						<?php if (isset($_SESSION['userid_dq'])) { ?>
						<li><a class="nav-link" href="validationQuestion.php">Proposer</a></li>
											<li><a class="nav-link" href="profil.php">Profil (<?= $_SESSION['pseudo_dq'] ?> <span id="ceinture-menu" class="ceinture2"></span> | <span id="directdollar-menu"></span> <img class='piecette' title='DirectDollar' src='images/dd.png'>)</a></li>
				  
					  <li><a class="nav-link" href="discordEndPoint.php"><image src="images/discord_chat.png"></image> Lier</a></li>
					  <li><a class="nav-link" href="logoff.php">Se déconnecter</a></li> <!-- Script qui reviendra accueil -->
						<?php } ?>
						
					   </ul>
                </div>
                <div class="search-box">
 
                </div>
            </div>
        </nav>
    </header>
    <!-- End header -->


    <div class="section layout_padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="full center">
                        <div class="heading_main text_align_center">
                            <h2><span class="theme_color">CLASSEMENT </span>GENERAL</h2>
                            <p class="large">Les meilleurs joueurs</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- section -->
    <div class="section layout_padding theme_bg">
        <div class="container">
            <div class="row">


                <div class="col-lg-12 col-md-12 col-sm-12 white_fonts">
             
                   <br/>
		<table id="classement" class="cell-border">
			<thead>
				<tr><th>#</th><th>Joueur</th><th>Points totaux</th><th>Meilleur score</th><th>Score moyen</th><th>Parties jouées</th><th>Dernière partie</th></tr>
			</thead>
		
			<tbody>   
				<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
			</tbody>
		</table>	   
				   
						<?php
						
						/*
							include('config.php');
							
							
							$COL_PSEUDO = 1;
							$COL_POINT_TOTAUX = 2;
							$COL_BEST_SCORE = 3;
							$COL_GAME_DONE = 4;
							
							$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
							$mysqli->set_charset("utf8mb4");
							
							if ($mysqli->connect_errno) {
								echo "Echec lors de la connexion : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
							}

								$query = "CALL DQ_GetCustomStatsUsers('all',".$COL_POINT_TOTAUX.", 'desc')";
								$result = $mysqli->query($query);
								
								echo '<table id="classement" class="cell-border">';
								echo '<thead>';
								echo '<tr><th>#</th><th>Joueur</th><th>Points totaux</th><th>Meilleur score</th><th>Score moyen</th><th>Parties jouées</th><th>Dernière partie</th></tr>';
								echo '</thead>';
								echo '<tbody>';
						
								
								$cpt = 1;
								
								while ($row = $result->fetch_array(MYSQLI_ASSOC))
								{

									if ($row['GameDone'] < 5)
									{
										$currentLevel = 0;
									}
									else if ($row['GameDone'] < 50)
									{
										$currentLevel = 1;
									}
									else if ($row['GameDone'] < 150)
									{
										$currentLevel = 2;
									}
									else if ($row['GameDone'] < 300)
									{
										$currentLevel = 3;
									}
									else if ($row['GameDone'] < 600)
									{
										$currentLevel = 4;
									}
									else if ($row['GameDone'] < 900)
									{
										$currentLevel = 5;
									}
									else if ($row['GameDone'] < 1200)
									{
										$currentLevel = 6;
									}
									else if ($row['GameDone'] < 2000)
									{
										$currentLevel = 7;
									}
									else if ($row['GameDone'] < 4000)
									{
										$currentLevel = 8;
									}
									else
									{
										$currentLevel = 9;
									}										

									$moyenne = round($row['Moyenne'], 0, PHP_ROUND_HALF_UP); 

									$lastGame = 'Date inconnue';
									if (!is_null($row['LastGame']))
									{
										$lastGame = strtotime($row['LastGame']);
										$lastGame = date('d/m/Y H:i:s', $lastGame);
									}
									
									echo '<tr><td>'.$cpt.'</td><td><img alt="avatar" class="avatar-classement" src="images/avatar/'.$row['UUID'].'.jpg?'.time().'" /><img alt="grade" class="avatar-classement-level" src="images/avatar/level_'.$currentLevel.'.png" /> '.$row['Pseudo'].'</td><td>'.$row['PointsTotaux'].'</td><td>'.$row['BestScore'].'</td><td>'.$moyenne.'</td><td>'.$row['GameDone'].'</td><td>'.$lastGame.'</td></tr>';
								
									$cpt++;
								}

								echo '</tbody>';
								echo '</table>';

							
								$result->free();

								
								$mysqli->close();
							
							
							$mysqli = null;
						*/
						?>
					 <br/>
				
                </div>
            </div>
        </div>
    </div>
    <!-- end section -->

	<?php 
	
		include('footer.php');
	
	?>

    <a href="#" id="scroll-to-top" class="hvr-radial-out"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.pogo-slider.min.js"></script>
    <script src="js/slider-index.js"></script>
    <script src="js/smoothscroll.js"></script>
    <script src="js/form-validator.min.js"></script>
    <script src="js/contact-form-script.js"></script>
    <script src="js/isotope.min.js"></script>
    <script src="js/images-loded.min.js"></script>
    <script src="js/custom.js"></script>
</body>

</html>