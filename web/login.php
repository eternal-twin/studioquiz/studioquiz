<?php
// login.php

require_once('credentials.php');

$authorizationUri = $oauthClient->getAuthorizationUri("base", "sign_in");
header("Location: " . $authorizationUri, true, 302);
exit;
?>