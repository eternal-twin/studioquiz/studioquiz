<?php
header('Content-type: text/html; charset=UTF-8');
include('config.php');

	$mysqli = new mysqli($ADRES, $USER, $MDP, $BASE);
	$mysqli->set_charset("utf8mb4");
	
	if ($mysqli->connect_errno) {
		echo "Echec lors de la connexion à MySQL : (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}

		$chaineFinale = "";

		$niveau = $_POST['niveau'];
		$theme = $_POST['theme'];
		$range = "30,64";
		if ($niveau == "1") $range = "0,35";
		else if ($niveau == "2") $range = "30,64";
		else if ($niveau == "3") $range = "60,100";
		else if ($niveau == "4") $range = "1,100";
		

		$query = "CALL DQ_Get10IDByDifficultyRange($range, $theme);";
		
		do {
			
			$IDs = array();
			
			$result = $mysqli->query($query);

			/* Tableau associatif */
			while ($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$IDs[] = $row['IDQuestion'];
			}

			/* Libération des résultats */
			$result->free();
		
		}
		while (count(array_unique($IDs,SORT_NUMERIC)) != 10);
		
		echo implode("###", $IDs);

		/* Fermeture de la connexion */
		$mysqli->close();
	
		$mysqli = null;
	
?>