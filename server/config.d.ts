export interface Config {
  /**
   * HTTPS config, or `undefined` for HTTP
   */
  https?: HttpsConfig

  /**
   * Discord client config
   */
  discord: DiscordConfig;

  eternaltwin: BotConfig;
  googleapis: GoogleapisConfig;
}

export interface HttpsConfig {
  /**
   * Path to the HTTPS key file
   */
  key: string;

  /**
   * Path to the HTTPS cert file
   */
  cert: string;
}

export interface DiscordConfig {
  /**
   * Discord client token
   */
  token: string;
}

export interface BotConfig {
  guild_id: string;
  channel_id: string;
  role: string;
}

export interface GoogleapisConfig {
  type: string;
  project_id: string;
  private_key_id: string;
  private_key: string;
  client_email: string;
  client_id: string;
  auth_uri: string;
  token_uri: string;
  auth_provider_x509_cert_url: string;
  client_x509_cert_url: string;
}

/**
 * Find the config from the current working directory and parse it.
 */
export function getLocalConfigSync(): Config;

/**
 * Parse the input config.
 */
export function parseConfig(input: string): Config;
