var socket;
var pseudoClient = "UNKNOWN";
var currentUUID = "";

var isConnected = false;

var audioIntroGame = new Audio('https://directquiz.niko.ovh/dev/intro-game.mp3');

var audioBonneReponse = new Audio('https://directquiz.niko.ovh/dev/bip_bonne_reponse.mp3');
var audioMauvaiseReponse = new Audio('https://directquiz.niko.ovh/dev/bip_mauvaise_reponse.mp3');
var audioLettre = new Audio('https://directquiz.niko.ovh/dev/bip_lettre.mp3');
var audioTicTac = new Audio('https://directquiz.niko.ovh/dev/tic_tac.mp3');
// var audioTac = new Audio('https://directquiz.niko.ovh/dev/tac.mp3');
var audioFin  = new Audio('https://directquiz.niko.ovh/dev/elapsed-modern.mp3');
var audioFlood  = new Audio('https://directquiz.niko.ovh/dev/bip_flood.mp3');
var audioQuestion  = new Audio('https://directquiz.niko.ovh/dev/question.mp3');

const fileLettre1 = "https://directquiz.niko.ovh/dev/lettre1.mp3";
const fileLettre2 = "https://directquiz.niko.ovh/dev/lettre2.mp3";
const fileLettre3 = "https://directquiz.niko.ovh/dev/lettre3.mp3";
const fileLettre4 = "https://directquiz.niko.ovh/dev/lettre4.mp3";
const fileLettre5 = "https://directquiz.niko.ovh/dev/lettre5.mp3";
const fileLettre6 = "https://directquiz.niko.ovh/dev/lettre6.mp3";

const sonsLettres = [
	new Audio(fileLettre1),
	new Audio(fileLettre2),
	new Audio(fileLettre3),
	new Audio(fileLettre4),
	new Audio(fileLettre5),
	new Audio(fileLettre6)
];

var audioDiDing = new Audio('https://directquiz.niko.ovh/dev/begin.mp3') 

var audioMsg = new Audio('https://directquiz.niko.ovh/dev/bip_msg.mp3');
var audioMusiqueChoix = new Audio('https://directquiz.niko.ovh/dev/musique_choix.mp3');

var audioApplause  = new Array(0);

var typingTimer;
var isTyping = false;

// FOND PLEIN : #f3feee
// FOND NORMAL : #d1f9fc
// FOND 10 Secondes restantes : #ffece4
var PROGRESS_BAR_BACK_COLOR_HIGH = "#f3feee";
var PROGRESS_BAR_BACK_COLOR_MEDIUM = "#d1f9fc";
var PROGRESS_BAR_BACK_COLOR_LOW = "#ffece4";

var PROGRESS_BAR_BACK_COLOR_ULTRALOW = "#ffabab";

// BARRE PLEIN : #8dd280
// BARRE NORMAL : #3d98d2
// BARRE 10 Secondes restantes : #eb7c70
var PROGRESS_BAR_COLOR_HIGH = "#8dd280";
var PROGRESS_BAR_COLOR_MEDIUM = "#3d98d2";
var PROGRESS_BAR_COLOR_LOW = "#eb7c70";

var PROGRESS_BAR_COLOR_ULTRALOW = "#f51313";

var currentApplause = -1;

var audioConnected  = new Audio('https://directquiz.niko.ovh/dev/connected.mp3');
var audioDisconnected  = new Audio('https://directquiz.niko.ovh/dev/disconnected.mp3');

var audioBlabla  = new Audio('https://directquiz.niko.ovh/dev/blabla.mp3');
const regexHTML = /(<([^>]+)>)/ig;

var actualChar = 0;
var currentText = "";

var currentQuestionOrTimeOut = "";
var intervaleChangephrase = null;
var annonceSpeaked = "";
var socket = null;

var newClassement = null;

var ranksTop = null;
var ranksLeft = null;

var points = null;

var rankingRestant = 8;

var dataMember = null;

var partieEnCours = true;

var currentSalon = 0;

var isOnline = true;

var currentpseudonyme = null;
var currentUUID = null;
var currentclientID = null;
var currentSalon = null;

var lastStateToIA = false;

var lettreHasard = null;

var timeoutMusiqueChoix = null;

var countPlayVoice = 0;
var countPlayVoice2 = 0;

/*
phrases5secondesRestantes = new Array(0);

phrases5secondesRestantes.push("Il ne reste plus que 5 secondes...");
phrases5secondesRestantes.push("Attention, seulement 5 secondes restantes !");
phrases5secondesRestantes.push("Depechez-vous, le temps est bientôt écoulé !");
phrases5secondesRestantes.push("5 secondes et le temps sera écoulé...");
phrases5secondesRestantes.push("Encore 5 secondes.");
phrases5secondesRestantes.push("Prudence, plus que 5 secondes !");
phrases5secondesRestantes.push("Plus que 5 secondes au chrono.");
phrases5secondesRestantes.push("Le temps est limité, 5 secondes restantes !");
phrases5secondesRestantes.push("Prudence, le temps restant n'est plus que de 5 secondes !");
phrases5secondesRestantes.push("Faites vite car il ne reste que 5 secondes !");
phrases5secondesRestantes.push("Le chrono s'arrète dans 5 secondes !");
phrases5secondesRestantes.push("5 secondes !");

function phrase5secondesAuHasard()
{
	
	return phrases5secondesRestantes[getRandomInt(0,phrases5secondesRestantes.length)];
	
}
*/

function playMusiqueChoix()
{
	
	audioMusiqueChoix = new Audio('https://directquiz.niko.ovh/dev/musique_choix.mp3');
	
	if (!isMuted) audioMusiqueChoix.play();
	
	timeoutMusiqueChoix = setTimeout(function(){
		
		playMusiqueChoix();
		
	},17479);
}

function stopMusiqueChoix()
{
	clearTimeout(timeoutMusiqueChoix);

	audioMusiqueChoix.pause();
	audioMusiqueChoix.currentTime = 0;
}

function getTypeMessage(IDAnnonce)
{
	switch (IDAnnonce)
	{
		case 1 : return '-connect'; 
		case 2 : return '-disconnect'; 
		case 3 : return '-good-answer';
		case 33 : return '-bad-answer';		
		case 4 : return '-presenter'; 
		case 5 : return '-info'; 
		case 6 : return '-info'; 
		case 7 : return '-url-forbidden'; 
		case 98 : return '-presenter'; 
		case 982 : return '-presenter'; 
		case 111 : return '-info';// DISCORD
		case 555 : return '-clap'; 
	}

}

for (i=0;i<24;i++)
{
	audioApplause.push(new Audio('https://directquiz.niko.ovh/dev/applause'+(i+1)+'.mp3'))
}

function getUUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function nextApplause()
{	
	if (currentApplause == 23) currentApplause = 0;
	else currentApplause++;
	
	return currentApplause;
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

String.prototype.normalizeAcc = function(){
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
     
    return str;
}

function connectToServer(pseudonyme, iduser, salon, clientID)
{
	dataMember = {
					id: iduser,
					pseudo: pseudonyme,
					client_id : clientID
				 };	

	if (!isConnected)
	{

			currentpseudonyme = pseudonyme;
			currentUUID = iduser;
			currentclientID = clientID;
			
			pseudonyme = pseudonyme.replace(/ /g,"");
			
			
			var port = 3000;
			var tokenSuffixe = "10";
			
			switch (salon)
			{
				case 1: port = 3000; isConnected = true; tokenSuffixe = "10"; break;
				case 2: port = 3001; isConnected = true; tokenSuffixe = "11"; break;
				case 3: port = 3002; isConnected = true; tokenSuffixe = "12"; break;
				case 4: port = 3003; isConnected = true; tokenSuffixe = "13"; break;
				case 5: port = 3004; isConnected = true; tokenSuffixe = "14"; break;
				case 6: port = 3005; isConnected = true; tokenSuffixe = "15"; break;
				case 7: port = 3006; isConnected = true; tokenSuffixe = "16"; break;
				case 8: port = 3007; isConnected = true; tokenSuffixe = "17"; break;
				case 9: port = 3008; isConnected = true; tokenSuffixe = "18"; break;
				case 10: port = 3009; isConnected = true; tokenSuffixe = "22"; break;
				case 11: port = 3010; isConnected = true; tokenSuffixe = "60"; break;
//++INSERTION_SALONS_1++
				case 25: port = 3025; isConnected = true; tokenSuffixe = "19"; break;
				case 91: port = 3091; isConnected = true; tokenSuffixe = "20"; break;
				case 99: port = 3050; isConnected = true; tokenSuffixe = "21"; break;
				default : port = 3000; isConnected = false; tokenSuffixe = "10";
			}
			
			socket = io.connect('www.directquiz.org:'+port, {
					query: {
						token: "3598bde5-abe2-4703-a481-384c5276f2"+tokenSuffixe,
						userData: JSON.stringify(dataMember)
					},
					transports: ["websocket"],
					pingTimeout: 4000,
					pingInterval: 6000
			});
	
			currentSalon = salon;
			var salonFrequentation = "";

			
			if (isConnected)
			{
			
				switch (currentSalon)
				{
					case 1 : salonFrequentation = "MEGAQUIZ"; break;
					case 2 : salonFrequentation = "SPECTACLE"; break;
					case 3 : salonFrequentation = "JEUX VIDÉO"; break;
					case 4 : salonFrequentation = "HISTOIRE"; break;
					case 5 : salonFrequentation = "GÉOGRAPHIE"; break;
					case 6 : salonFrequentation = "INFORMATIQUE"; break;
					case 7 : salonFrequentation = "SPORT"; break;
					case 8 : salonFrequentation = "MEGAQUIZ ENGLISH"; break;
					case 9 : salonFrequentation = "MEGAQUIZ NEDERLANDS"; break;
					case 10 : salonFrequentation = "SCIENCES"; break;
					case 11 : salonFrequentation = "GASTRONOMIE"; break;
//++INSERTION_SALONS_2++
					case 91: salonFrequentation = "HISTOIRE DU NUMÉRIQUE ET DE L'INTERNET"; 	
				}
				
				
				
				
				$.post("/niko.ovh/directquiz89/insertFrequentation.php",{UUID : currentUUID, Salon : salonFrequentation}).done(function(data){
					
					if (data == 'OK')
					{
						console.log("stats saved !");
					}
					else
					{
						console.error("issue when saving stats...");
					}
					
				});
			
			}				
				
			socket.on('message', newMessage);
			socket.on('annonce_vocale', newAnnonceVocale);
			socket.on('annonce', newAnnonce);
			socket.on('annonce_user', newAnnonceUser);
			socket.on('slots', newListeSlots);
			socket.on('slotsClassement', newListeSlotsClassement);
			// socket.on('checkDisconnect', checkDisconnect);		
			socket.on('ack', acknowledge);
			socket.on('refreshTopMessage', refreshTopMessage);
			socket.on('refreshPropositions', refreshPropositions);
			
			socket.on('chrono', updateChrono);
			socket.on('annonce_flood_user', floodGet);
			
			socket.on('applaudissement_send', applaudissement_send);
			socket.on('applaudissement_get', applaudissement_get);
			
			socket.on('status_game_get', status_game_get);

			socket.on('notify_points',notifyPoints);
			socket.on('notify_ordre',notifyOrdre);
			
			socket.on('reset_affichage_classement', resetAffichageClassement);

			socket.on('kick', kick);		
			socket.on('forcer_ejection', forcerEjection);
			
			socket.on('refresh_status_writing', refreshStatusWriting);
			
			socket.on('suspect_id_question', suspectIDQuestion);
			
			socket.on("connect", () => {
				
			  if(socket.disconnected)
			  {
				  disconnected();
			  }
			  else
			  {
				  console.log("Connexion établie !");
				  
				
					  setTimeout(function(){
						  
						if ($('#questionbox-content').text().trim() == "") 
						{
							playMusiqueChoix();
						}
						  
					  },2500);
				  
			  }
			  
			});
						
			// socket.on('are_alive', areAlive);
			
			// socket.emit('addMember',dataMember);
			
			pseudoClient = pseudonyme;
	
			
	
	}
}

/*
function areAlive(data)
{
	if (data.value == 1)
	{
		socket.emit('is_alive',{uuid:currentUUID});
	}
}
*/

function quizEnCours()
{
	socket.emit('status_game',{msg:0});

	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(partieEnCours);
		}, 2000);
	});
}

function disconnected()
{
	$("#button-start").show();
	isStart = false;
	error("Serveur innaccesible... / Vous avez été déconnecté...");
}

function status_game_get(data)
{
	partieEnCours = data.statusGame;
	
	if (!partieEnCours)
	{
		// setRepondu(false);	
	}
	
}

function kick(data)
{
	if (IDConnected == data.id)
	{
		window.location = "https://www.directquiz.org/index.php?type=kick";
	}
}

function displayTexteProgressif() {

		actualChar = 0;
	    intervaleChangephrase = setInterval('changeMessage()', 90);

}

function numeroLettreAuHasard() {
  return Math.floor(Math.random() * 6) + 1;
}

function audioLettreAuHasard()
{
	
	let currentNumeroLettre = numeroLettreAuHasard();
	
	lettreHasard = new Audio("https://directquiz.niko.ovh/dev/lettre"+currentNumeroLettre+".mp3");
	lettreHasard.play();
	
}

function changeMessage() {
	     if (actualChar < currentQuestionOrTimeOut.length) {
	         currentText += currentQuestionOrTimeOut.charAt(actualChar);	      
	         actualChar++;
			 if (!isMuted) audioLettreAuHasard();
	     }
	     $('#questionbox-content').text(currentText);
		 
		 if (actualChar == currentQuestionOrTimeOut.length)
		 {
			 clearInterval(intervaleChangephrase);			 
		 }
}
	
function suspecterLaQuestionCourante()
{
	socket.emit('getQuestionIDCurrent',{uuid:currentUUID});
}
	
function error(message)
{
	  Lobibox.alert('error', {
            msg: "<div style='color: red;'>" + message.replace(/D\$/g,"<img class='piecette' title='DirectDollar' src='images/dd.png' />") + "</div>"
      });
}

function info(message)
{
	  Lobibox.alert('info', {
            msg: "<div style='color: darkblue;'>" + message.replace(/D\$/g,"<img class='piecette' title='DirectDollar' src='images/dd.png' />") + "</div>"
      });
}		
	
function suspectIDQuestion(data)
{	

	if (data.uuid == currentUUID && data.idQuestion > 0)
	{
		var questionToCanSignal = ((currentSalon!=8)?'Etes-vous sûr de vouloir signaler la question suivante : '+data.libelleQuestion:'Are your sure to report this question : '+data.libelleQuestion);
		

							Lobibox.confirm({
									msg: "<div style='color: darkblue;'>" + questionToCanSignal + "</div>",
									callback: function(lobibox, type){
										if (type === 'yes'){

											signalerByID(data.idQuestion, data.libelleQuestion);

										}

									}
								});

	}
	else if (data.uuid == currentUUID)
	{
		if (currentSalon!=8)
		{	
			switch (data.idQuestion)
			{
				case -2: error("Une erreur d'ordre inconnu est survenue, réessayez plus tard..."); break;
				case -3: error("Il n'y a pas de partie en cours pour le moment...");
			}
		}
		else
		{
			switch (data.idQuestion)
			{
				case -2: error("A unknown error appear, try again later..."); break;
				case -3: error("There is no game at this moment...");
			}
		}
	}
}	 

function signalerByID(idQuestion, libelleQuestion)
{
	
		$.post("niko.ovh/directquiz89/setSuspectByID.php",{id:idQuestion}).done(function(data){
			
			if (currentSalon!=8)
			{
				info("La question \""+libelleQuestion+"\" a été signalée avec succès !");
			}
			else
			{
				info("The question question \""+libelleQuestion+"\" has been report successfully !");
			}
		
		}).
		fail(function(){
			
			if (currentSalon!=8)
			{
				error("Une erreur d'ordre inconnu est survenue lors du signalement de la question !");
			}
			else
			{
				error("A unknown error appear while the reporting of the question !");
			}	
			
		});
	
}

function messageEnter(e, uuid) {

	if (e.keyCode == 13 && $("#message-send").val().trim().replace(/ /g,"") != "") {

			var data = {
				id: uuid,
				pseudo: pseudoConnected,
				message: $("#message-send").val(),
				ia: $("#toIA").prop("checked")
					};
			write(data);

			return false;
  }
		 
}
	 
function write(data)
{
	
	newMessageClient(data);
	
	if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") == -1)
	{	
		if (data.message.indexOf("/gif") == -1 && data.message != "/mute"  && data.message != "/unmute" && data.message != "/cls" && data.message.indexOf("/setvoice") == -1) socket.emit('message',data);	
	}	
	
	$("#message-send").val("");
	
}

function acknowledge(data)
{

	if (data.msg == "ERROR_TOOMANYUSER")
	{
		window.location = '8max.php';
	}
	else if (data.msg == "OK")
	{
		$("#currentConnected").text(pseudoClient);
		$("#connectFrame").hide();
		$("#sender").show();
	}
	
}

function refreshTopMessage(data)
{
	if (data.nickname == pseudoClient) $("#questionbox-content").html(data.msg);
}

function melanger(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];  // échange des éléments
    }
    return array;
}

function sendReponse(reponseVoulue, idRepondant)
{
		var data = {
					id: idRepondant,
					pseudo: pseudoConnected,
					message: "/requestXG450 "+reponseVoulue,
					ia: false
			};
		write(data);
}

function refreshPropositions(data)
{
	var propositionsPossibles = [];
	
	if (data.nickname == "###ALL###" || data.nickname == pseudoClient)
	{
		propositionsPossibles.push(data.p1);
		propositionsPossibles.push(data.p2);
		propositionsPossibles.push(data.p3);
		propositionsPossibles.push(data.p4);
	
		afficherPropositionsPourRepondre(propositionsPossibles[0], propositionsPossibles[1], propositionsPossibles[2], propositionsPossibles[3]);
	
	}
}

function floodGet(data)
{
	if (data.user == pseudoClient)
	{
		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.message + "</div>");
		$("#chatbox").scrollTop(99999);
		setRepondeurBot();	
		audioFlood.play();
	}	
}

function updateChrono(data)
{
	// data.restant
	// data.total
				if (parseInt(data.restant,10) < 40)
				{
					if (parseInt(data.restant,10) == 0)
					{
						if (!isMuted) audioFin.play();
					}
					else
					{
						if (!isMuted)
						{
							audioTicTac = new Audio('https://directquiz.niko.ovh/dev/tic_tac.mp3');
							
							if (parseInt(data.restant,10)%2 == 0) audioTicTac.play();
							
							
							// if (parseInt(data.restant,10) == 5) presentateurParle(phrase5secondesAuHasard());
						}
						
					}
				}
			
			var color_bar_progress = "#8dd280";
			var color_back_progress = "#f3feee";
			
			if (data.restant <= 40 && data.restant >= 30)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_HIGH;
				color_bar_progress = PROGRESS_BAR_COLOR_HIGH;
			}
			else if (data.restant <= 29 && data.restant >= 11)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_MEDIUM;
				color_bar_progress = PROGRESS_BAR_COLOR_MEDIUM;
			}
			else if (data.restant >= 6 && data.restant <= 10)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_LOW;
				color_bar_progress = PROGRESS_BAR_COLOR_LOW;
			}
			else if (data.restant >= 0 && data.restant <= 5)
			{
				color_back_progress = PROGRESS_BAR_BACK_COLOR_ULTRALOW;
				color_bar_progress = PROGRESS_BAR_COLOR_ULTRALOW;
			}			
				$(".progress-circle").css("border-color",color_back_progress) // Couleur du fond de l'anneau
				$(".progress-barre").css("border-color",color_bar_progress) // Couleur de la progress bar
				$(".progress-sup50").css("border-color",color_bar_progress) // Couleur de la progress bar

			
		$(".progress-circle").each(function(){			
			$(this).attr("data-value",(data.restant < 10?"0":"")+data.restant);	

			if (data.restant == 0)
			{
				setTimeout(function(){
					
					$(this).attr("data-value","40");
					
				},1000);
			}			
		}); 
		

	

}

function nettoyerMessageDeToutesSesInsultes(message)
{
	insultes = new Array(0);
	
insultes.push("MOTHER FUCKER");
 insultes.push("FILS DE PUTE");
 insultes.push("TA MÉRE LA PUTE");
 insultes.push("TA MÈRE LA PUTE");
 insultes.push("TA MERE LA PUTE");
 insultes.push("FILS DE CHIEN");
 insultes.push("BORDEL");
 insultes.push("SAPERLIPOPETTE");
 insultes.push("SALOPERIE");
 insultes.push("SALOPE");
 insultes.push("CUL");
 insultes.push("BITE");
 insultes.push("CHIER");
 insultes.push("PUTAIN");
 insultes.push("MERDEUX");
 insultes.push("MERDE"); 
 insultes.push("CONNARD");
 insultes.push("CONNASSE");
 insultes.push("CONNE");
 insultes.push("CONS"); 
 insultes.push("CON");
 insultes.push("ENCULES");
 insultes.push("ENCULE");
 insultes.push("ENCULER");
 insultes.push("FOUTRE");
 insultes.push("PUTE");
 insultes.push("PUTTE");
 insultes.push("SALOPE");
 insultes.push("ABRUTI"); 
 insultes.push("MORVEUX");
 insultes.push("CRETIN");
 insultes.push("IMBECILE");
 insultes.push("FUCK"); 
 insultes.push("COUILLES");
 insultes.push("COUILLE");
 insultes.push("ZIZI");
 insultes.push("ZEZETTE");
 insultes.push("KIKINE");

	var sentence = message;

		// Remplace chaque terme insultant par des étoiles
		  for (let i = 0; i < insultes.length; i++) {
			const censored = "*".repeat(insultes[i].length);
			sentence = sentence.replace(new RegExp("\\b" + insultes[i] + "\\b", "gi"), censored);
			}

	  return sentence;

}

function newAnnonceUser(data)
{

	if (data.user == IDConnected)
	{

		if (data.type == 3)
		{
			if (!isMuted) audioBonneReponse.play();
			
			if (data.annonce.indexOf("Bonne réponse ! ") == 0 && currentSalon != 8)
			{
				$("#chatbox > .message").last().remove();
			}
			else if (data.annonce.indexOf("Good answer ! ") == 0 && currentSalon == 8)
			{
				$("#chatbox > .message").last().remove();
			}
		}
		else if (data.type == 33)
		{
			if (!isMuted) audioMauvaiseReponse.play();			
		}		

		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
		$("#chatbox").scrollTop(99999);
		
	}
}

function applaudissement_send(data)
{
	if (data.user == IDConnected)
	{
		
		$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
		$("#chatbox").scrollTop(99999);
		
		if (!isMuted)
		{
			audioApplause[nextApplause()].play();
		}
			
		$(".player[uuid='"+data.user+"'] > .player-pseudo").addClass('shake-slow');		

		setTimeout(function(){
			
			$(".player[uuid='"+data.user+"'] > .player-pseudo").removeClass('shake-slow');	
			
		},3900);
	}
}

function applaudissement_get(data)
{
	$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + data.annonce + "</div>");
	$("#chatbox").scrollTop(99999);
	
	
	
		if (!isMuted) audioApplause[nextApplause()].play();
	
		$(".player[uuid='"+data.user+"'] > .player-pseudo").addClass('shake-slow');	
		
		setTimeout(function(){
			
			$(".player[uuid='"+data.user+"'] > .player-pseudo").removeClass('shake-slow');	
			
		},3900);
}


function newAnnonceVocale(data)
{
	
	duree = data.secondes;


		/*
		
			setTimeout(function(){
				animParler();
				setTimeout(function(){
				animStop();
		
			},duree*1000);
			},1850);
		
		*/

	if (!isMuted)
	{
		setTimeout(function(){
			
			var annonceAudio = new Audio('https://directquiz.org/speech/' + data.annonce);
			
			// Ajoute un événement d'écoute pour détecter les changements d'état de lecture
			annonceAudio.addEventListener('play', function() {
			  countPlayVoice++;
			  animParler();
			  lastStateToIA = $("#toIA").prop("checked");
			  $("#toIA").prop("checked",false);
			  $("#toIA").prop("disabled",true); 
			});

			annonceAudio.addEventListener('ended', function() {
			  countPlayVoice--;	
			  if (countPlayVoice == 0)
			  {
				  animStop();
				  $("#toIA").prop("checked",lastStateToIA);
				  $("#toIA").prop("disabled",false);
			  }  
			});
			
			annonceAudio.volume = 1;
			annonceAudio.play();

			
		},350);
	}
	else
	{
		setTimeout(function(){
			
			var annonceAudio = new Audio('https://directquiz.org/speech/' + data.annonce);
			
			// Ajoute un événement d'écoute pour détecter les changements d'état de lecture
			annonceAudio.addEventListener('play', function() {
			  countPlayVoice2++;
			  animParler();
			  lastStateToIA = $("#toIA").prop("checked");
			  $("#toIA").prop("checked",false);
			  $("#toIA").prop("disabled",true);		  	  
			});

			annonceAudio.addEventListener('ended', function() {				
			  countPlayVoice2--;			  
			  if (countPlayVoice2 == 0)
			  {			
					animStop();
					$("#toIA").prop("checked",lastStateToIA);
					$("#toIA").prop("disabled",false);			  
			  }
			  
			});
			
			annonceAudio.volume = 0;
			annonceAudio.play();
			
		},350);		
	}
	
}

// BALISE VIDEO

function animParler() {
    // Lit la vidéo
    lecteur.play();
}

function animStop() {
    // Arrête la vidéo
    // On met en pause
    // On se remet au départ
    lecteur.currentTime = 0;
    lecteur.pause();

}

// -- BALISE VIDEO



function newAnnonce(data)
{
	var annonce = "";
	
	if (data.type != 111 && data.type != 982)
		annonce = decodeURIComponent(data.annonce.replace(regexHTML,""));
	else
		annonce = data.annonce;
	
	annonce = annonce.replace("お茶が好き","Oka ga suki");
	
	
	var dureeMS = 4000; //(data.annonce.length * 100) - 850;
	
	if (data.type == 4)
	{
		currentText = "";
		
		
		if (annonce.indexOf('A :') == -1)
		{
			currentQuestionOrTimeOut = annonce;
		}
		
		if (annonce == "Le quiz est terminé ! Merci à tous pour votre participation !")
		{
			
			if (!isMuted) playMusiqueChoix();
			
			if ($(".choice").length > 0)
			{
				setHoverChoices();

				$(".choice").each(function(){

					$(this).html("&nbsp;");
					$(this).removeAttr("title");
					
				});
			}
		}
		
		$("#questionbox-content").removeClass();
		
		if (annonce.indexOf("La bonne réponse était") > -1 || annonce.indexOf("The correct answer was") > -1)
		{
			
			$("#questionbox").animate({			
					'background-color': (currentSalon!=91?'#f05274':'#4e51ff') 
			},700,function(){
				
			});

		}
		else
		{
			if (annonce.indexOf("QUESTION") > -1)
			{
				if (!isMuted) audioQuestion.play();
				$("#questionbox").animate({			
					'background-color': (currentSalon!=91? '#a12e7a':'#2e2685')   
				},700,function(){
						
				});
			}
			else
			{
					$("#questionbox").animate({			
						'background-color': (currentSalon!=91? '#ba348d':'#3c33b6')     
					},700,function(){
						
					});
					
					if (annonce.indexOf("Une partie en mode ") > -1 || annonce.indexOf("A game in") > -1)
					{
						if (!isMuted) 
						{
							setTimeout(function(){
						
								stopMusiqueChoix();
						
								audioIntroGame.play();
								
								setTimeout(function(){
									
									audioDiDing.play();
									
								}, 10000);
								
							},1700);
							
						}
					}
			}

		}
		
		if (annonce.indexOf('A :') == -1)
		{
			displayTexteProgressif();
		}
		
					
		
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											//animParler();
		
		/*
		if (annonce.length <= 10)
		{
			dureeMS = 900;
		}
		*/
		
		if (annonce.indexOf("La bonne réponse était") > -1 || annonce.indexOf("The correct answer was") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}

		
	}
	else if (data.type == 3)
	{
		if (!isMuted) audioBonneReponse.play();
	}
	else if (data.type == 5)
	{
		if (annonce.lastIndexOf(pseudoClient+" ") > -1)
		{
			location.reload();
		}
	}
	else if (data.type == 99)
	{
		
		if (annonce.indexOf("La bonne réponse était") > -1 || annonce.indexOf("The correct answer was") > -1)
		{
			annonceSpeaked = annonce.toLowerCase();
		}
		else
		{
			annonceSpeaked = annonce;
		}
		
		/*
		$("#avatarAnimateur").html('<video id="animation-presentateur" muted preload="auto" autoplay="true" loop width="288" height="216">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.webm"'+
											'type="video/webm">'+
											'<source src="https://directquiz.niko.ovh/dev/img/animateur.mp4"'+
											'type="video/mp4">'+
											'</video>');
											*/
											
											//animParler();
		
	}
	else if (data.type == 98 || data.type == 982)
	{
		
		setTimeout(function(){
			
			if (annonce.indexOf("Discord Officiel") > -1 || annonce.indexOf("Official Discord") > -1)
			{
					annonce = annonce.replace("Discord Officiel","<a href='https://discord.com/invite/ERc3svy' target='_BLANK'><img src='images/discord_chat.png' alt='Discord Officiel'></a>").replace("Official Discord","<a href='https://discord.com/invite/ERc3svy' target='_BLANK'><img src='images/discord_chat.png' alt='Official Discord'></a>");
					$("#chatbox").append("<div class='message-discord'> <img src='images/icon-presenter"+(currentSalon == 2 || currentSalon == 25 || currentSalon == 11 ?"_F":"")+".png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>" + annonce + "</p></div>");
					$("#chatbox").scrollTop(99999);
			}
			else
			{
					$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'> <img src='images/icon-presenter"+(currentSalon == 2 || currentSalon == 25 || currentSalon == 11 ?"_F":"")+".png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>" + annonce + "</p></div>");
					$("#chatbox").scrollTop(99999);
			}

		},1900);

		//animParler();
		
	}
	
	if (data.type != 98 && data.type != 982)
	{
		const uuidCurrent = getUUID();
		
		if (getTypeMessage(data.type) == "-url-forbidden")
		{
			$("#chatbox").append("<div dataref='"+uuidCurrent+"' class='message"+getTypeMessage(data.type)+"'><img src='images/icon-interdit.png' alt='' class='message-info-img'/> <p class='message-disconnect-label'>"+annonce+"</p></div>");		
			$("div[dataref='"+uuidCurrent+"']").prev().remove();
		}
		else if (getTypeMessage(data.type) == "-info")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-info.png' alt='' class='message-info-img'/> <p class='message-info-label'>"+annonce+"</p></div>");		
		}
		else if (getTypeMessage(data.type) == "-connect")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-connect.png' alt='' class='message-connect-img'/> <p class='message-connect-label'>"+annonce+"</p></div>");		
		}
		else if (getTypeMessage(data.type) == "-disconnect")
		{
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'><img src='images/icon-disconnect.png' alt='' class='message-disconnect-img'/> <p class='message-disconnect-label'>"+annonce+"</p></div>");		
		}
		else
		{	
			$("#chatbox").append("<div class='message"+getTypeMessage(data.type)+"'>" + annonce + "</div>");	
		}
		
		$("#chatbox").scrollTop(99999);
		setRepondeurBot();	
		
		if (data.type == 1) // connexion
		{
			if (!isMuted) audioConnected.play()
		}
		else if (data.type == 2) // déconnexion
		{
			if (!isMuted) audioDisconnected.play()
		}
	}
	
	$( "img" ).load(function() {
			$("#chatbox").scrollTop(99999);	
	});
}

function presentateurParle(annonceParlee)
{		
		$("#chatbox").append("<div class='message"+getTypeMessage(98)+"'> <img src='images/icon-presenter"+(currentSalon == 2 || currentSalon == 25 || currentSalon == 11 ?"_F":"")+".png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>" + annonceParlee + "</p></div>");
		$("#chatbox").scrollTop(99999);
}

function newMessage(data)
{
	
	if (!isMuted) audioMsg.play();
	
	var message = "";
	
	if (data.message.indexOf('giphy.com/media/') > -1)
	{
		 message = data.message;
	}
	else
	{
		 message = data.message.replace(regexHTML,"");
	}	
	
	$("#chatbox").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+uuid()+"' class='microavatar' /> <strong>" + (data.pseudo.split("#")[1] == "1"?data.pseudo.replace("#1",""):data.pseudo) + "</strong> " + (data.ia?'<img src="images/toIA.png" class="imgToIA" alt="vers présentateur" />':'') + " : " + message + "</div>");
	
	$( ".gif" ).load(function() {
			$("#chatbox").scrollTop(99999);	
	});
	
	$("#chatbox").scrollTop(99999);	
	setRepondeurBot();
}

function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        const r = (Math.random() * 16) | 0;
        const v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}

function setFocusToEndInputMessage()
{
	// Sélectionnez l'élément HTML sur lequel vous souhaitez donner le focus
	var elem = document.getElementById("message-send");

	// Donnez le focus à l'élément
	elem.focus();

	// Obtenez la longueur du texte dans l'élément
	var length = elem.value.length;

	// Placez le curseur à la fin de l'élément
	elem.setSelectionRange(length, length);
}

function setRepondeurBot()
{
	// Récupérer tous les éléments de la page
	var $respondBot1 = $(".bot1");
	var $respondBot2 = $(".bot2");
	var $respondBot3 = $(".bot3");

	$(".pseudoBot").css("color","darkgreen");
	$(".pseudoBot").css("text-decoration","underline");
	$(".pseudoBot").css("cursor","pointer");

	// Parcourir les éléments et ajouter le gestionnaire d'événements au clic
	$respondBot1.each(function(){
		
		$(this).click(function(){	
			$("#message-send").val('@BOT_1 ');
			setFocusToEndInputMessage();
		});
		
	});
	
	// Parcourir les éléments et ajouter le gestionnaire d'événements au clic
	$respondBot2.each(function(){
		
		$(this).click(function(){		
			$("#message-send").val('@BOT_2 ');
			setFocusToEndInputMessage();
		});
		
	});

	// Parcourir les éléments et ajouter le gestionnaire d'événements au clic
	$respondBot3.each(function(){
		
		$(this).click(function(){		
			$("#message-send").val('@BOT_3 ');
			setFocusToEndInputMessage();
		});
		
	});	
		
}

function afficherFrequentationsIdeales()
{
	
	$.get("/niko.ovh/directquiz89/getMeilleuresFrequentations.php").done(function(data){
		
		let item1 = data; //"Il y a plus de fréquentations de joueurs de "+data[0].Quartier+ ", de "+data[1].Quartier+" et de " + data[2].Quartier + ", revenez vers ces heures si vous avez personne...";
		$("#chatbox").append("<div class='message-info'>" + item1 + "</div>");
		
	});

}

$(function(){
	
  
  /*
    $.fn.fallingFlowers = function(options) {
    var settings = $.extend({
      speed: 10000, // Vitesse de chute
      density: 2100, // Densité de fleurs
      windIntensity: 60 // Intensité du vent
    }, options);

    var screenWidth = $(window).width();

    return this.each(function() {
      var $container = $(this);

      setInterval(function() {
        var startX = Math.random() * screenWidth;
        var endX = Math.random() * screenWidth;
        var $flower = $("<div class='flower"+idFleur()+" flower'></div>").css({
          left: startX,
          top: -30
        });

        $container.append($flower);

        $flower.animate({
          top: $(window).height(),
          left: endX
        }, settings.speed, function() {
          $(this).fadeOut(1800);
        });

        $flower.animateRotate(Math.random() * settings.windIntensity - (settings.windIntensity / 2));
      }, settings.density);
    });
  };

  $.fn.animateRotate = function(angle, duration, easing, complete) {
    return this.each(function() {
      var $elem = $(this);

      $({deg: 0}).animate({deg: angle}, {
        duration: duration || 1000,
        easing: easing || 'swing',
        step: function(now) {
          $elem.css({
            transform: 'rotate(' + now + 'deg)'
          });
        },
        complete: complete || $.noop
      });
    });
  };
*/

/*
  $("body").fallingFlowers({
    speed: 10000,
    density: 2100,
    windIntensity: 60
  });

  $(window).scroll(function() {
    $(".flower").each(function() {
      if ($(this).offset().left > ($(window).width() - 300)) {
        $(this).fadeOut(1800);
      }
    });
  });
*/
  
/*
	if (currentSalon == 1)
	{
		setTimeout(function(){
			
				setInterval(function(){
					
					$.ajax({
					url: "https://www.directquiz.org/ping.php",
						error: function(jqXHR, textStatus, errorThrown){					
					
							if (textStatus == 'error')
							{
									isOnline = false;
									$("#chatbox").append("<div class='message"+getTypeMessage(2)+"'> <img src='images/icon-disconnect.png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>Connexion au serveur perdue, tentative de reconnexion...</p></div>");
									$("#chatbox").scrollTop(99999);
							}
						
					},
						success: function(data){					
											
							if (!isOnline)
							{
								isOnline = true;
								$("#chatbox").append("<div class='message"+getTypeMessage(1)+"'> <img src='images/icon-connect.png' alt='' class='message-presenter-img'> <p class='message-presenter-label'>Connexion retrouvée, reconnexion...</p></div>");
								$("#chatbox").scrollTop(99999);
								setTimeout(function(){
								connectToServer(currentpseudonyme, currentUUID, currentSalon, currentclientID);
								},4000);
							}
						
					},
						timeout: 2000 // sets timeout to 2 seconds
					});				
						
				},6000);
				

					
		},1000);
	}
*/

// Gestion du realtime typing current

	$("#message-send").keypress(function () {
		if (currentSalon > 0)
		{
			  clearTimeout(typingTimer);
			  if (!isTyping) 
			  {
					var dataWriting = {
					uuid: currentUUID,
					state: 1
					};	
					
				socket.emit('currentWriting',dataWriting);
				isTyping = true;
			  }
		}
	  
	});

	$("#message-send").keyup(function () {
		if (currentSalon > 0)
		{
			  clearTimeout(typingTimer);
			  typingTimer = setTimeout(function () {
				if (isTyping) {
					var dataWriting = {
					uuid: currentUUID,
					state: 0
					};	
					
				 socket.emit('currentWriting',dataWriting);
				 isTyping = false;
				}
			  }, 900);
		}
	});
	
	afficherFrequentationsIdeales();
	
});


function idFleur() {
  return Math.floor(Math.random() * 3) + 1;
}

function refreshStatusWriting(data)
{
	statusText = "";
	
	console.log("DATA : ==" + data.id[0] + "==" +data.id[1] +"==");

	var listeFinale = new Array(0);

	for (i = 0; i < data.id.length; i++)
	{
		if (data.id[i] != currentUUID) listeFinale.push(data.id[i]);
	}
	
	for (i = 0; i < listeFinale.length; i++)
	{
		
		var separateur = ", ";
		
		if (i == listeFinale.length-2)
		{
			separateur = " et ";
		}
		
		if (!(i == listeFinale.length-1))
		{
			statusText += $(".player[uuid='"+listeFinale[i]+"']").children(".player-pseudo").text() + separateur;
		}
		else
		{
			statusText += $(".player[uuid='"+listeFinale[i]+"']").children(".player-pseudo").text();
		}
		
	}

	var verbe = ((currentSalon!=8)?"est":"is");
	var cod = ((currentSalon!=8)?"en train d'écrire":"writing");
	
	if (listeFinale.length > 1) verbe = ((currentSalon!=8)?"sont":"are");
	
	if (listeFinale.length > 0)
	$("#writingCurrentContainer").html("<img src='images/loadingWriting.gif'>&nbsp;<span>" + statusText + " " + verbe + " "+cod+"...</span>");
	else
	$("#writingCurrentContainer").html("");

}

function newMessageClient(data)
{
	
		if (data.message.indexOf("GIF_[1bdcdd9a-4682-48a2-aeb4-6ba65b98d1a5]") > -1)
		{
			var GIF = data.message.split(/\|/);
			$("#chatbox").append("<div class='message'> <img src='images/avatar/"+data.id+".jpg?"+uuid()+"' class='microavatar' /> <strong>" + data.pseudo + "</strong> : " + "<img class='gif' src='"+GIF[1]+"' />" + "</div>");
			
			var dataGIF = {
				id: IDConnected,
				pseudo: pseudoConnected,
				message: "<img class='gif' src='"+GIF[1]+"' />"
			};

			socket.emit('message',dataGIF);
			
			$( ".gif" ).load(function() {
				$("#chatbox").scrollTop(99999);	
			});
		}
		else
		{	
	
		
				var message = nettoyerMessageDeToutesSesInsultes(data.message.replace(regexHTML,""));	
				
				if (message == "/cls") $("#chatbox").html("");
				
				if (message.indexOf("/setvoice") == 0)
				{
					if (message.indexOf(" femme") > -1)
					{
						//setFemme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix de femme !");
					}
					else if (message.indexOf(" homme") > -1)
					{
						//setHomme();
						//if (!isMuted) presentateurParle("Bonjour ! Je vous parle en voix d'homme !");
					}
				}
				else if (message.indexOf("/gif") == 0)
				{
					var query = message.replace("/gif",""); 
					if (query != "") searchGIF(query);	
				}
				else
				{
					//$(".slot[data-user-id="+IDConnected+"]").children(".pseudonyme").eq(0).text()
					if (message.indexOf("/start") == -1 && message.indexOf("/addbot") == -1 && message.indexOf("/clearbot") == -1 && message.indexOf("/requestXG450") == -1)
					{
						$("#chatbox").append("<div class='message'><img src='images/avatar/"+IDConnected+".jpg?"+uuid()+"' class='microavatar' /> <strong>" + dataMember.pseudo + "</strong> "+(data.ia?'<img src="images/toIA.png" class="imgToIA" alt="vers présentateur" />':'')+" : " + message + "</div>");
						if (!isMuted) audioMsg.play();
					}
					
				}
				$("#chatbox").scrollTop(99999);
				setRepondeurBot();				
		}
	
	
}

function itemClassement(uuid, points)
{
	this.uuid = uuid;
	this.points = points;
}

function newListeSlotsClassement(data)
{

	points = data; // uuid, points
	
	reorganiserClassement(true);

}

function notifyPoints(data)
{

	if (dataMember.id == data.uuid)
	{
		$("#score-good-answer-points").text("+"+data.points)
		
		$("#score-good-answer").show('bounce', {}, 250,function(){});
		
		setTimeout(function(){
			
			$("#score-good-answer").fadeOut(250);
			
		},2800)
		
		
	}
}

function notifyOrdre(data)
{

	var uuidToSet = data.uuid;
	var ordreToSet = data.ordre;

	$(".player[uuid='"+uuidToSet+"']").addClass("player-good-answer");
	$(".player[uuid='"+uuidToSet+"']").children(".player-good-answer-notification").text(ordreToSet);
	$(".player[uuid='"+uuidToSet+"']").children(".player-good-answer-notification").show();
	
}

function resetAffichageClassement(data)
{
	$(".player").each(function(){
		
			$(this).removeClass("player-good-answer");
			$(this).children(".player-good-answer-notification").hide();
		
	});
}

function newListeSlots(data)
{
	var UUIDAdmin = "514c0686-1d07-4f44-aa21-01f44c8a00be";
	var htmlClassementContent = "";
	
	points = new Array(0);
	
	for (i = 0; i < data.length; i++)
	{

		htmlClassementContent += '<div style="position:absolute;" uuid="'+data[i].id+'" class="player">'+
							 '<div style="display:none;" class="player-good-answer-notification"></div>'+
							 '<img src="images/avatar/'+data[i].id+'.jpg" alt="" uuid="'+data[i].id+'" class="player-avatar">'+
							 '<img src="images/avatar/level_1.png" uuid="'+data[i].id+'" class="avatar-level" />'+
							 '<p class="player-pseudo">'+ (data[i].id == UUIDAdmin?'<i class="couronne" title-data="ADMIN" style="font-style: normal; cursor: help;">👑</i>':'') +' '+(data[i].version == 1?data[i].name:data[i].name+"#"+data[i].version)+'</p>'+
						     '<span class="pts-negatif"> '+data[i].points+' </span>'+
						     '</div>';
	
		points.push(new itemClassement(data[i].id, data[i].points));		 
							 
	}
	
	$("#score").html("");
	$("#score").append("<div id='classement-liste'>" + htmlClassementContent + "</div>");
	$("#score").append('<div style="display:none" id="score-good-answer">'+
					   '<span id="score-good-answer-points"></span>'+
					   '<span id="score-good-answer-label">Points</span>'+
					   '</div><div style="display:none;" id="tooltip"></div>');
				
	$("#classement-liste").css("position:relative;");

	
				$(".couronne").mousemove(function(event){

					$("#tooltip").text($(this).attr("title-data"));
					
					$("#tooltip").offset({ top: event.pageY+15, left: event.pageX+15 });
				
				});
				
				$(".couronne").mouseleave(function(event){
					
					$("#tooltip").hide();
					
				});
				
				$(".couronne").mouseenter(function(event){
					
					$("#tooltip").show();
					$(this).effect( "highlight", {}, 400, function(){ } );
					
				});
	
	
	
	// ranksTop contient dans l'ordre du classement les positions top 
	// ranksLeft contient dans l'ordre du classement les positions left
	ranksTop = new Array(0);
	ranksLeft = new Array(0);
	
	for (i = 0; i < points.length; i++)
	{
		var rankIndex = i + 1;
		
		var ranktop = 0
		var rankleft = 0
		
		if (rankIndex >= 1 && rankIndex <= 4) rankleft = 0;
		if (rankIndex >= 5 && rankIndex <= 8) rankleft = 209;
		
		switch(rankIndex)
		{
			case 1:ranktop=0; break;
			case 2:ranktop=43; break;
			case 3:ranktop=86; break;
			case 4:ranktop=129; break;
			case 5:ranktop=0; break;
			case 6:ranktop=43; break;
			case 7:ranktop=86; break;
			case 8:ranktop=129;
		}
		
		ranksTop.push(ranktop);
		ranksLeft.push(rankleft);
	}	
					   
	reorganiserClassement(false);				   


}


function reorganiserClassement(isAnimated)
{

	pointsOrdre = points.map((x) => x);
	
	pointsOrdre.sort(function (a, b) {
			return b.points - a.points;
	});
	
		rankingRestant = points.length;
		processReorganisation(rankingRestant, rankingRestant, isAnimated);
	
	refreshAvatars();
	
}

function getPositionClassementByTopLeft(top_, left_)
{
	
	if (top_ == 0)
	{
			if (left_ == 0)
			{
				return 1;
			}
			else if (left_ == 209)
			{
				return 5;
			}
	}
	else if (top_ == 43)
	{
			if (left_ == 0)
			{
				return 2;
			}
			else if (left_ == 209)
			{
				return 6;
			}
	}
	else if (top_ == 86)
	{
			if (left_ == 0)
			{
				return 3;
			}
			else if (left_ == 209)
			{
				return 7;
			}
	}
	else if (top_ == 129)
	{
			if (left_)
			{
				return 4;
			}
			else if (left_ == 209)
			{
				return 8;
			}
	}
	
}

function processReorganisation(restant, total, isAnimated)
{
	
		if (restant > 0)
		{
			
			var indexItem = (total-(restant-1))-1;

			// uuidCurrent est l'UUID de l'utilisateur au fur et à mesure de l'ordre du classement final dans l'ordre
			var uuidCurrent = pointsOrdre[indexItem].uuid;
			
			// Placement au premier plan de l'item courant 
			$(".player[uuid='"+uuidCurrent+"']").css("z-index",999);
			
			// currentTop et currentLeft sont la position actuelle des utilisateurs au fur et à mesure dans l'ordre du classement final
			var currentTop = $(".player[uuid='"+uuidCurrent+"']").position().top;
			var currentLeft = $(".player[uuid='"+uuidCurrent+"']").position().left;
			
			var rank = getPositionClassementByTopLeft(currentTop, currentLeft);
			
			if (isAnimated)
			{
				
				// Animation du player dont l'uuid est celui à classer en première position et ainsi de suite (basé sur pointsOrdre)
				
				// ranksTop/Left = La position que l'on souhaite appliquer en fonction du rang voulu
				// Par rapport au RANK actuel qui est calculé grace au top et au left actuel
				$(".player[uuid='"+uuidCurrent+"']").animate({
						top: ranksTop[indexItem], //- (43*(rank-1)) + (rank >= 5 && rank <= total?172:0),
						left : ranksLeft[indexItem] //- (rank >= 5 && rank <= total?209:0)
					}, 500, function() {
	
				});	
				
				//
				//rank >= 5 && rank <= total?(-209):0)
				
			}
			else
			{
				// identique à l'animation mais sans animation
				$(".player[uuid='"+uuidCurrent+"']").css("top",ranksTop[indexItem]).css("left",ranksLeft[indexItem]);
			}


			// - (43*(rank-1)) + (rank >= 5 && rank <= total?172:0)
			//  + (rank >= 5 && rank <= total?(-209):0)

			restant--; // décrémentation au fur et à mesure (normalement pas de bug)
			processReorganisation(restant, total, isAnimated); //Recursivité (normalement ok)
			
		}
		else
		{
			
			// Attribution des points en fin d'animation, normalement aucun soucis de bugs...			
			for (i = 0; i < pointsOrdre.length; i++)
			{
				$(".player[uuid='"+pointsOrdre[i].uuid+"']").children("span").text(pointsOrdre[i].points);		
			}			

			return;
		}
}